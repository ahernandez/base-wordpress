<?php
/*  inc
==============================================================================*/
require TEMPLATEPATH. '/inc/general.php';
// require TEMPLATEPATH. '/inc/shortcodes.php';
// require TEMPLATEPATH. '/inc/wp_bootstrap_navwalker.php';

require TEMPLATEPATH. '/inc/template-tags.php';
require TEMPLATEPATH. '/inc/extras.php';
require TEMPLATEPATH. '/inc/jetpack.php';


/*  admin
==============================================================================*/
// require TEMPLATEPATH. '/admin/backend.php';



/*  Custom post y Taxonomy
==============================================================================*/

// Descomentar la linea de abajo, para utilizar custom post - Agenda-Slider-etc
// require TEMPLATEPATH. '/inc/custom-post-agenda.php';


// Activa la página enlace en el administrador
add_filter( 'pre_option_link_manager_enabled', '__return_true' );


/* Registrar menu */
register_nav_menu( 'primary-menus', __( 'Primary Menus', 'arielhf' ) );


add_action ( 'wp_enqueue_scripts', 'add_style' );
function add_style(){}

add_action ( 'wp_enqueue_scripts', 'add_script' );
function add_script(){}
?>
