<?php get_header(); ?>

<section id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

	<?php if ( have_posts() ) : ?>

		<header class="page-header">
			<h2 class="page-title"><?php printf( esc_html__( 'Resultados para: %s', 'evision-corporate' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
		</header><!-- .page-header -->

		<?php while ( have_posts() ) : the_post(); ?>

			<?php
			get_template_part( 'template-parts/content', 'search' );
			?>

		<?php endwhile; ?>

		<?php  the_posts_navigation(); ?>

	<?php else : ?>

		<?php get_template_part( 'template-parts/content', 'none' ); ?>

	<?php endif; ?>

	</main><!-- #main -->
</section><!-- #primary -->


<?php get_footer(); ?>
